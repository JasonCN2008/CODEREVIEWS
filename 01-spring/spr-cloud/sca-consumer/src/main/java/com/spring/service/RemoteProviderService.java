package com.spring.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "sca-provider",contextId ="remoteProviderService" )
public interface RemoteProviderService {

     @GetMapping("/provider/echo/{data}")
     String doEcho1(@PathVariable("data") String data);
}
