package com.spring.controller;

import com.spring.service.RemoteProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer/")
public class ConsumerController {
    @Value("${server.port}")
    private String server;
    @Autowired
    private RemoteProviderService remoteProviderService;
    @GetMapping("/echo1")
    public String doEcho1(){
        return remoteProviderService.doEcho1(server);
    }
}
