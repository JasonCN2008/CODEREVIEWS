## 如何理解Spring Framework？

Spring Framework是一个资源整合框架，它可以整合所有优秀资源，以高效、低耗、低耦合的方式对外提供服务。简化我们自己编写代码的过程，提高工作效率。

## 如何理解Spring中的IOC设计？

IOC是一种设计思想，我们称之为控制反转，在Spring框架中基于这种IOC思想创建对象、管理对象、以及对象与对象之间的依赖关系。然后以更加科学有效的方式，降低我们应用对象的成本。Spring中的IOC实现会基于反射方式进行依赖注入(DI)，实现对象与对象之间的解耦，更好提高代码的灵活性、可扩展性。

## 如何理解Spring中的Bean对象？

首先，它是一个Java对象，这个Java对象可以交给Spring创建和管理。其次，Spring可以赋予这些Bean对象一些更加科学的特性。例如延迟加载，作用域，生命周期方法，属性的赋值，我们可以基于这些特性，在内存中更好的应用这些对象来解决业务问题。

## Spring为Bean对象赋予了哪些特性？

Spring管理的Bean，为其提供了作用域、延迟加载、生命周期方法、依赖注入等特性。其中：

作用域主要分为Singleton和Prototype两种，Singleton为单例作用域，名字相同的bean在内存中只能有一份。Prototype作用域为多例作用域，每次从spring获取这个作用域的对象都会创建一个新的对象，但spring框架不负责此作用域对象的销毁。

延迟加载又称之为懒加载，可以在应用对象时再创建此对象，通常会配合单例作用域使用。因为Prototype作用域的对象默认支持延迟加载特性。

Spring管理的Bean可以在类的内部，通过@PostConstruct和@PreDestroy注解定义Bean对象的生命周期方法。其中@PostConstruct注解定义生命周期初始化方法，这个方法会在对象构建完成时执行。@PreDestroy注解用于描述生命周期销毁方法，对于单例作用域的对象，会在对象销毁之前，执行此方法。

依赖注入(DI)是在程序运行时，由spring框架按照某种机制为对象属性或方法参数进行赋值，其主要目的就是降低耦合提高代码的可扩展性。

## 如何理解Spring中的@Autowried注解？

此注解一般用于描述属性、构造方法、set方法等。用于告诉系统底层，按照一定的规则去查找与属性类型或方法参数类型匹配的对象。假如有类型匹配的对象并且只有一个，则直接进行值的注入。假如有多个类型相同的Bean对象，系统底层还会按属性名或方法参数名找对应类型的对象然后进行值的注入，假如没有名字相同的Bean对象，也可以基于@Qualifier注解指定要注入的Bean的名字。假如也没有指定，系统底层会抛出异常。



## 如何理解Spring中的AOP设计？

首先，AOP是一种设计思想，我们称之为面向切面编程思想，其目的是在不修改目标代码或少量修改的情况下，为目标业务织入拓展功能，可以理解为锦上添花。例如日志记录、事务处理、权限控制、缓存应用等，其次，AOP基于IOC设计，底层逻辑是在目标业务对象创建时，基于代理模式为目标业务对象创建代理对象，然后通过代码对象为目标业务进行功能增强。



## Spring中的AOP有哪些核心对象？

代理对象（CGLIB,JDK）,目标业务对象，切面对象(Aspect),通知方法，切入点、连接点，注解对象。

定义切面的注解:@Aspect

定义切面顺序的注解：@Order

定义切入点的注解：@PointCut

定义通知方法的注解：@Before,@After,@AfterReturning,@AfterThrowing,@Around

启动AOP配置的注解：@EnableAspectJAutoProxy



## 如何理解Spring MVC？

Spring MVC 是Spring Framework中的一个Web模块，这个模块基于MVC分而治之的设计思想，对请求响应处理的过程进行了封装。可以以一种更简单的方式处理请求和响应，简化我们处理请求和响应的过程，提高工作效率。

## Spring MVC中有哪些核心对象？

过滤器(Filter),控制器(Servlet),映射处理器(HandlerMapping),拦截器(Interceptor),处理器(Handler通常也称之为Controller)，响应值对象(ModelAndView),视图解析器(ViewResolver)等

## Spring MVC 中常用注解有哪些？



定义Handler组件的注解：@Controller/@RestController

定义请求方式，API，参数相关的：

@RequestMapping/@GetMapping/@PostMapping/@PutMapping/@PatchMapping/@DeleteMapping

@RequestParam/@PathVariable/@RequestBody

@ResponseBody

@ControllerAdvice/@RestControllerAdvice/@ExceptionHandler











































