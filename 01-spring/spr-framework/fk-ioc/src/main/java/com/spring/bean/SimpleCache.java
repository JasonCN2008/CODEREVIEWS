package com.spring.bean;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Scope("prototype")//prototype为多例作用域，默认是延迟加载，每次从spring获取的对象都是新创建的对象
//@Scope("singleton")//默认单例作用域
//@Lazy //延迟加载(懒加载)，对象使用时再创建，默认是服务启动时就创建，通常会配合单例作用域使用
public class SimpleCache implements Cache{
    public SimpleCache(){
        System.out.println("SimpleCache()");
    }

    /**
     *  @PostConstruct 用于定义Bean对象的初始化方法，
     *  这个方法会在bean对象创建完成以后执行
     */
    @PostConstruct
    public void init(){
        System.out.println("init()");
    }

    /**
     *  @PreDestroy用于定义Bean对象的销毁方法，
     *  此方法会在单例对象从容器移除之前执行。
     *  对于多例对象prototype，spring只负责创建
     *  不负责销毁。
     */
    @PreDestroy
    public void destory(){
        System.out.println("destory()");
    }
}
