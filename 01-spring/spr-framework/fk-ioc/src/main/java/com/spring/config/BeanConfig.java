package com.spring.config;

import com.example.Container;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class BeanConfig {
    @Bean //这里bean的名字默认为方法名
    //@Bean("container")
    public Container container(){
        return new Container();
    }
}
