package com.spring;

import com.spring.bean.SoftCache;
import com.spring.bean.WeakCache;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

public class CacheTests {
    private AnnotationConfigApplicationContext context;
    @Before
    public void init(){
        context= new AnnotationConfigApplicationContext(IocApplication.class);
    }
    @Test
    public void testSoftCache(){
        SoftCache softCache = context.getBean("softCache", SoftCache.class);
        System.out.println(softCache);
    }
    @Test
    public void testWeakCache(){
        WeakCache weakCache = context.getBean("weakCache", WeakCache.class);
        System.out.println(weakCache);
    }
}
