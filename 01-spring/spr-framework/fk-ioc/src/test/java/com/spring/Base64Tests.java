package com.spring;

import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64Tests {

    @Test
    public void testBase64(){
        String content="hello";
        //获取Base64编码对象
        Base64.Encoder encoder = Base64.getEncoder();
        //对内容进行编码
        byte[] encode = encoder.encode(content.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(encode));

        //获取Base64解码对象
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decode = decoder.decode(encode);
        System.out.println(new String(decode));
    }
}
