package com.spring.service;

public interface ArticleService {
     Object selectById(Long id);
}
