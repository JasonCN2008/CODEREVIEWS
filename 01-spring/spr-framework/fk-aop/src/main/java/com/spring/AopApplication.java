package com.spring;

import com.spring.service.ArticleService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@ComponentScan
@EnableAspectJAutoProxy
public class AopApplication {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx=
                new AnnotationConfigApplicationContext(AopApplication.class);
        ArticleService articleService =
                ctx.getBean("articleServiceImpl", ArticleService.class);
        Object result = articleService.selectById(10L);

    }
}
