package com.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot构建于Spring基础之上，其目的就是要简化开发过程，提高工作效率。
 * 主要实现了如下特性：
 * 1)起步依赖(spring-boot-starter-web,.....)
 * 2)自动配置(WebMvcAutoConfiguration,...)
 * 3)嵌入式服务(tomcat)
 * 4)健康检查(actuator)
 */

@SpringBootApplication
public class BootApplication {
    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }
}
